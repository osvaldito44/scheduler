#include<stdio.h>
#include<conio.h>
#define max 30
void main()
{
	int i,n,j,t,bt[max],wt[max],pr[max],tat[max],pos;
	float awt=0, atat=0;
	//clrscr();
	printf("**********************************************\t\n");
	printf("**  Scheduler                               **\t\n");
	printf("**  Ingrese el numero de Procesos:          **\t\n");
	scanf("%d",&n);
	printf("**  Ingresa los procesos:                   **\t\n");
	for(i=0;i<n;i++)
	{
		scanf("%d",&bt[i]);
	}
	printf("**  Ingresa la prioridad de los procesos:   **\t\n");
	for(i=0;i<n;i++)
	{
		scanf("%d",&pr[i]);
	}
	for(i=0;i<n;i++)
	{
		pos=i;
		for(j=i+1;j<n;j++)
		{
			if(pr[j]<pr[pos])
			{
				pos=j;
			}
		}
		t=pr[i];
		pr[i]=pr[pos];
		pr[pos]=t;

		t=bt[i];
		bt[i]=bt[pos];
		bt[pos]=t;
	}


	wt[0]=0;
	printf("No. Proceso\t Procesos\t Prioridad\t\n");
	for(i=0;i<n;i++)
	{
		tat[i]=wt[i]+bt[i];
		awt=awt+wt[i];
		atat=atat+tat[i];
		printf("%d\t\t%d\t\t%d\n",i+1,bt[i],pr[i]);
	}
	awt=awt/n;
	atat=atat/n;
	printf("Tiempo de Espera= %f\n",awt);
	printf("Tiempo de Respuesta= %f\n",atat);
	getch();
}